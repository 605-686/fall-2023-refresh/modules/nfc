---
title: NFC
---

## Introduction

!!! note

    Note that these videos are from 2016 and were based in Java using Android Views for the UI. I have updated the example to Kotlin using Jetpack Compose for the UI.
    The concepts in the lecture and example videos still apply. (It also might be interesting to watch these older videos to see how Android development looks in Java compared to Kotlin).

    Also note that these examples will not run on the emulator; you can only use NFC on a device that supports it. I would have loved to include an assignment on NFC, but not all students
    will have Android devices, or Android devices with NFC.

Near-Field Communication (NFC) allows your application to read and write hardware tags with a small chunk of data. This data can be plain text, a URI, contact information, or any other data format you would like.
This can be useful, for example, to read data from a poster or business card or write data to a bag describing its contents.

## Videos

Total video time for this module: 50:53

            
### Near-Field Communication: Lecture (Spring 2016) (19:11)

<iframe width="800" height="450" src="https://www.youtube.com/embed/wXEjjljdNWI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Near-Field Communication: Reading Example (Spring 2016) (16:16)

<iframe width="800" height="450" src="https://www.youtube.com/embed/QzphwRdJ7r0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Near-Field Communication: Writing Example (Spring 2016) (15:26)

<iframe width="800" height="450" src="https://www.youtube.com/embed/mUVdV9EmIQs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/605-686/fall-2023-refresh/modules/nfc](https://gitlab.com/605-686/fall-2023-refresh/modules/nfc)